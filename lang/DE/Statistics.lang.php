<?php
global $g_lang;

# StatisticsView
// getStatCities
$g_lang['active_cities'] = 'Aktivste Bezirke';
// getStatTotal
$g_lang['stat_whole'] = 'Gesamtstatistik';
// getStatFoodsaver
$g_lang['most_active_foodsavers'] = 'Aktivste Foodsaver';
